import React from 'react';
import {useDispatch} from "react-redux";
import GoogleLoginButton from 'react-google-login';
import {googleClientId} from "../../../config";
import {Button} from "@material-ui/core";
import {googleLogin} from "../../../store/actions/usersActions";
import {FcGoogle} from "react-icons/fc";

const GoogleLogin = () => {
	const dispatch = useDispatch();

	const handleLogin = response => {
		dispatch(googleLogin(response));
	};

	return (
		<GoogleLoginButton
			clientId={googleClientId}
			render={props => (
				<Button
					color="inherit"
					onClick={props.onClick}
					startIcon={<FcGoogle/>}
				>
					Login with Google
				</Button>
			)}
			onSuccess={handleLogin}
			cookiePolicy={'single_host_origin'}
		/>
	);
};

export default GoogleLogin;