import React from 'react';
import {useDispatch} from "react-redux";
import path from "path";
import {Button, Grid, makeStyles} from "@material-ui/core";
import defaultAvatar from "../../../../assets/images/default_avatar.png";
import {apiURL} from "../../../../config";
import {logoutUser} from "../../../../store/actions/usersActions";
import {Link} from "react-router-dom";

const useStyles = makeStyles({
	avatar: {
		width: '35px',
		height: '35px',
		borderRadius: '50%'
	},
});

const UserMenu = ({user}) => {
	const classes = useStyles();
  const dispatch = useDispatch();

	let avatar = defaultAvatar;
	const extensions = ['.png', '.jpg', 'jpeg', '.svg', '.gif'];

	if (user.avatar) {
		if (extensions.includes(path.extname(user.avatar))) {
			avatar = apiURL + '/' + user.avatar;
		} else {
			avatar = user.avatar;
		}
	}

  return (
    <Grid container justifyContent="space-between" alignItems="center">
	    <img src={avatar} alt="Avatar" className={classes.avatar}/>
      <Button aria-controls="simple-menu" aria-haspopup="true" color="inherit">
       {user.displayName}!
      </Button>
	    {(user && user.role === 'user') &&<Button component={Link} to='/my-cocktails' color="inherit">My cocktails</Button>}
	    <Button onClick={() => dispatch(logoutUser())} color="inherit">Logout</Button>
    </Grid>
  );
};

export default UserMenu;