import React from 'react';
import {Link} from "react-router-dom";
import {Button, Grid} from "@material-ui/core";
import GoogleLogin from "../../GoogleLogin/GoogleLogin";

const AnonymousMenu = () => {
  return (
    <Grid item container justifyContent="flex-end" alignItems="center">
	    <GoogleLogin/>
      <Button component={Link} to="/admin" color="inherit">Admin</Button>
    </Grid>
  );
};

export default AnonymousMenu;