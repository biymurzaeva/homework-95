import React from 'react';
import {AppBar, Grid, makeStyles, Toolbar, Typography} from "@material-ui/core";
import {useSelector} from "react-redux";
import UserMenu from "./Menu/UserMenu";
import AnonymousMenu from "./Menu/AnonymousMenu";
import {Link} from "react-router-dom";

const useStyles = makeStyles(theme => ({
  mainLink: {
    color: "inherit",
    textDecoration: 'none',
    '$:hover': {
      color: 'inherit'
    }
  },
  staticToolbar: {
    marginBottom: theme.spacing(2)
  },
	appBar: {
		zIndex: theme.zIndex.drawer + 1,
	},
}));

const AppToolbar = () => {
  const classes = useStyles();
  const user = useSelector(state => state.users.user);

  return (
    <>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
	        <Grid container justifyContent="space-between" alignItems="center">
		        <Grid item>
			        <Typography variant="h6">
				        <Link to="/" className={classes.mainLink}>Cocktails</Link>
			        </Typography>
		        </Grid>
		        <Grid item>
			        <Grid container justifyContent="flex-end" alignItems="center">
				        {user ? (
					        <Grid item>
						        <UserMenu user={user}/>
					        </Grid>
				        ) : (
					        <AnonymousMenu/>
				        )}
			        </Grid>
		        </Grid>
	        </Grid>
        </Toolbar>
      </AppBar>
      <Toolbar className={classes.staticToolbar}/>
    </>
  );
};

export default AppToolbar;