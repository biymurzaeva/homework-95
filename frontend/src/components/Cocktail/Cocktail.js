import React from 'react';
import {Button, Card, CardActions, CardContent, CardMedia, Grid, makeStyles, Typography} from "@material-ui/core";
import {apiURL} from "../../config";
import {Link} from "react-router-dom";
import {useDispatch} from "react-redux";
import {deleteCocktail, publishCocktail} from "../../store/actions/cocktailsActions";

const useStyles = makeStyles(() => ({
	card: {
		height: '100%',
		display: 'flex',
		flexDirection: 'column',
	},
	cardMedia: {
		paddingTop: '115.25%',
	},
	cardContent: {
		flexGrow: 1,
	},
	cardAction: {
		display: "flex"
	},
}));

const Cocktail = props => {
	const classes = useStyles();
	const dispatch = useDispatch();

	let cardImage = null;

	if (props.image) {
		cardImage = apiURL + '/' + props.image;
	}

	return (
		<Grid item xs={12} sm={6} md={4}>
			<Card className={classes.card}>
				<CardMedia
					className={classes.cardMedia}
					image={cardImage}
					title="Image title"
				/>
				<CardContent className={classes.cardContent}>
					<Typography gutterBottom variant="h5" component="h2">
						{props.name}
					</Typography>
				</CardContent>
				<CardActions className={classes.cardAction}>
					<Button color="primary" component={Link} to={`/cocktails/${props.id}`}>View</Button>
					{(props.publish === false) && <Button color="secondary" onClick={() => dispatch(publishCocktail(props.id))}>To publish</Button>}
					{(props.role === 'admin') && <Button color="secondary" onClick={() => dispatch(deleteCocktail(props.id))}>Delete</Button>}
				</CardActions>
			</Card>
		</Grid>
	);
};

export default Cocktail;