import React, {useState} from 'react';
import FormElement from "../UI/Form/FormElement";
import {Button, Container, Grid, makeStyles, TextField} from "@material-ui/core";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles(theme => ({
	paper: {
		marginTop: theme.spacing(8),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	form: {
		marginTop: theme.spacing(1),
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
	ing: {
		display: "flex",
		margin: "15px 0",
	},
}));

const CocktailForm = ({error, loading, onSubmit}) => {
	const classes = useStyles();

	const [ingredients, setIngredients] = useState([{title: '', amount: ''}]);

	const [cocktail, setCocktail] = useState({
		name: '',
		image: null,
		recipe: '',
		ingredients: null,
	});

	const changeIngredient = (i, name, value) => {
		setIngredients(prevState => {
			const ingCopy = {
				...prevState[i],
				[name]: value,
			};

			return prevState.map((ingredient, index) => {
				if (i === index) {
					return ingCopy;
				}

				return ingredient;
			})
		});
	};

	const addIngredient = () => {
		setIngredients(prevState => [
			...prevState,
			{title: '', amount: ''}
		]);
	};

	const deleteIngredient = i => {
		const newIngredients = [...ingredients];

		if (newIngredients.length !== 1) {
			newIngredients.splice(i, 1);
		}
		setIngredients(newIngredients);
	};

	const inputChangeHandler = e => {
		const {name, value} = e.target;
		setCocktail(prevState => ({...prevState, [name]: value}));
	};

	const fileChangeHandler = e => {
		const name = e.target.name;
		const file = e.target.files[0];
		setCocktail(prevState => {
			return {...prevState, [name]: file};
		});
	};

	const getFieldError = fieldName => {
		try {
			return error.errors[fieldName].message;
		} catch (e) {
			return undefined;
		}
	};

	const submitFormHandler = e => {
		e.preventDefault();

		const formData = new FormData();

		Object.keys(cocktail).forEach(key => {
			if (key === 'ingredients') {
				return formData.append(key, JSON.stringify(ingredients));
			}
			formData.append(key, cocktail[key]);
		});

		onSubmit(formData);
	};

	return (
		<Container component="section" maxWidth="sm">
			<div className={classes.paper}>
				<Grid
					component="form"
					container
					className={classes.form}
					onSubmit={submitFormHandler}
					spacing={2}
					noValidate
				>
					<FormElement
						type="text"
						required
						label="Name"
						name="name"
						value={cocktail.name}
						onChange={inputChangeHandler}
						error={getFieldError('name')}
					/>

					<Grid item xs={12}>
						{ingredients.map((ingredient, i) => (
							<div className={classes.ing} key={i}>
								<FormElement
									type="text"
									required
									label="Title"
									name="title"
									value={ingredients[i].title}
									onChange={e => changeIngredient(i, 'title', e.target.value)}
									error={getFieldError(`ingredients.${i}.title`)}
								/>
								<FormElement
									type="text"
									required
									label="Amount"
									name="amount"
									value={ingredients[i].amount}
									onChange={e => changeIngredient(i, 'amount', e.target.value)}
									error={getFieldError(`ingredients.${i}.amount`)}
								/>
								<Button onClick={() => deleteIngredient(i)}>x</Button>
							 </div>
						))}
						<Button variant="contained" onClick={addIngredient}>Add ingredient</Button>
					</Grid>

					<FormElement
						multiline
						rows={4}
						label="Recipe"
						name="recipe"
						onChange={inputChangeHandler}
						value={cocktail.recipe}
						error={getFieldError('recipe')}
					/>

					<Grid item xs>
						<TextField
							type="file"
							name="image"
							required
							onChange={fileChangeHandler}
							error={Boolean(getFieldError('image'))}
							helperText={getFieldError('image')}
						/>
					</Grid>

					<Grid item xs={12}>
						<ButtonWithProgress
							type="submit"
							fullWidth
							variant="contained"
							color="primary"
							className={classes.submit}
							loading={loading}
							disabled={loading}
						>
							Create
						</ButtonWithProgress>
					</Grid>
				</Grid>
			</div>
		</Container>
	);
};

export default CocktailForm;