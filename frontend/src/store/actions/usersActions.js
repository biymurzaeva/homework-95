import usersSlice from "../slices/usersSlices";

export const {
	registerUser,
	registerUserFailure,
	registerUserSuccess,
	loginUser,
	loginUserSuccess,
	loginUserFailure,
	logoutUser,
	clearErrorUser,
	googleLogin,
} = usersSlice.actions;
