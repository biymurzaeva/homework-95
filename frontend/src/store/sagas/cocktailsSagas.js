import {call, put, takeEvery} from "redux-saga/effects";
import {
	createCocktail,
	createCocktailError,
	createCocktailSuccess,
	deleteCocktail,
	deleteCocktailFailure,
	deleteCocktailSuccess,
	fetchCocktail,
	fetchCocktailFailure,
	fetchCocktails,
	fetchCocktailsFailure,
	fetchCocktailsSuccess,
	fetchCocktailSuccess,
	fetchUserCocktails,
	fetchUserCocktailsFailure,
	fetchUserCocktailsSuccess,
	publishCocktail,
	publishCocktailFailure,
	publishCocktailSuccess,
	rateCocktail, rateCocktailFailure,
	rateCocktailSuccess
} from "../actions/cocktailsActions";
import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {historyPush} from "../actions/historyActions";

export function* createCocktailSaga({payload: cocktailData}) {
	try {
		yield axiosApi.post('/cocktails', cocktailData);
		yield put(createCocktailSuccess());
		yield call(historyPush('/'))
		toast.success('Cocktail created');
	} catch (error) {
		yield put(createCocktailError(error.response.data));
	}
}

export function* fetchCocktailsSaga() {
	try {
		const response = yield axiosApi.get('/cocktails');
		yield put(fetchCocktailsSuccess(response.data));
	} catch (error) {
		yield put(fetchCocktailsFailure(error.response.data.error));
	}
}

export function* fetchUserCocktailsSaga({payload: id}) {
	try {
		const response = yield axiosApi.get(`/cocktails?user=${id}`);
		yield put(fetchUserCocktailsSuccess(response.data));
	} catch (error) {
		yield put(fetchUserCocktailsFailure(error.response.data.error));
	}
}

export function* fetchCocktailSaga({payload: id}) {
	try {
		const response = yield axiosApi.get(`/cocktails/${id}`);
		yield put(fetchCocktailSuccess(response.data));
	} catch (error) {
		yield put(fetchCocktailFailure(error.response.data.error));
	}
}

export function* deleteCocktailSaga({payload: id}) {
	try {
		yield axiosApi.delete(`/cocktails/${id}`);
		yield put(deleteCocktailSuccess(id));
		toast.success('Cocktail deleted');
	} catch (error) {
		yield put(deleteCocktailFailure(error.response.data.error));
	}
}

export function* publishCocktailSaga({payload: id}) {
	try {
		const cocktail = yield axiosApi.put(`/cocktails/${id}`);
		yield put(publishCocktailSuccess({id, cocktail : cocktail.data}));
		toast.success('Cocktail published');
	} catch (error) {
		yield put(publishCocktailFailure(error.response.data));
		toast.error('You do not have permission to publish');
	}
}

export function* rateCocktailSaga({payload: data}) {
	try {
		yield axiosApi.post(`/cocktails?cocktail=${data.id}`, data.rate);
		yield put(rateCocktailSuccess());
	} catch (error) {
		yield put(rateCocktailFailure(error.response.data));
		toast.error('You need register');
	}
}

const cocktailsSagas = [
	takeEvery(createCocktail, createCocktailSaga),
	takeEvery(fetchCocktails, fetchCocktailsSaga),
	takeEvery(fetchUserCocktails, fetchUserCocktailsSaga),
	takeEvery(fetchCocktail, fetchCocktailSaga),
	takeEvery(deleteCocktail, deleteCocktailSaga),
	takeEvery(publishCocktail, publishCocktailSaga),
	takeEvery(rateCocktail, rateCocktailSaga),
];

export default cocktailsSagas;