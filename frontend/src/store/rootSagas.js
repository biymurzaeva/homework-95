import {all} from 'redux-saga/effects';
import registerUserSaga from "./sagas/usersSagas";
import createCocktailSaga from "./sagas/cocktailsSagas";

export function* rootSagas() {
	yield all([
		...registerUserSaga,
		...createCocktailSaga,
	]);
}