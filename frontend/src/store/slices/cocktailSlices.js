const {createSlice} = require('@reduxjs/toolkit');

const name = 'cocktail';

const cocktailSlice = createSlice({
	name,
	initialState: {
		cocktails: [],
		cocktail: {},
		userCocktails: [],
		createCocktailLoading: false,
		createCocktailError: null,
		fetchCocktailsLoading: false,
		fetchCocktailsError: null,
		fetchUserCocktailsLoading: false,
		fetchUserCocktailError: null,
		fetchCocktailLoading: false,
		fetchCocktailError: null,
		deleteCocktailLoading: false,
		deleteCocktailError: null,
		publishError: null,
		rateCocktailError: null,
	},
	reducers: {
		createCocktail(state) {
			state.createCocktailLoading = true;
		},
		createCocktailSuccess(state) {
			state.createCocktailLoading = false;
			state.createCocktailError = null;
		},
		createCocktailError(state, action) {
			state.createCocktailLoading = false;
			state.createCocktailError = action.payload;
		},
		fetchCocktails(state) {
			state.fetchCocktailsLoading = true;
		},
		fetchCocktailsSuccess(state, action) {
			state.fetchCocktailsLoading = false;
			state.fetchCocktailsError = null;
			state.cocktails = action.payload;
		},
		fetchCocktailsFailure(state, action) {
			state.fetchCocktailsLoading = false;
			state.fetchCocktailsError = action.payload;
		},
		fetchUserCocktails(state) {
			state.fetchUserCocktailsLoading = true;
		},
		fetchUserCocktailsSuccess(state, action) {
			state.fetchUserCocktailsLoading = false;
			state.userCocktails = action.payload;
		},
		fetchUserCocktailsFailure(state, action) {
			state.fetchUserCocktailsLoading = false;
			state.fetchUserCocktailError = action.payload;
		},
		fetchCocktail(state) {
			state.fetchCocktailLoading = true;
		},
		fetchCocktailSuccess(state, action) {
			state.fetchCocktailLoading = false;
			state.fetchCocktailError = null;
			state.cocktail = action.payload;
		},
		fetchCocktailFailure(state, action) {
			state.fetchCocktailLoading = false;
			state.fetchCocktailError = action.payload;
		},
		deleteCocktail(state) {
			state.deleteCocktailLoading = true;
		},
		deleteCocktailSuccess(state, action) {
			state.deleteCocktailLoading = false;
			state.deleteCocktailError = null;
			state.cocktails = state.cocktails.filter(cocktail => cocktail._id !== action.payload);
		},
		deleteCocktailFailure(state, action) {
			state.deleteCocktailLoading = false;
			state.deleteCocktailError = action.payload;
		},
		publishCocktail() {},
		publishCocktailSuccess(state, action) {
			state.cocktails = state.cocktails.map(cocktail => {

				if (cocktail._id === action.payload.id) {
					return action.payload.cocktail;
				}

				return cocktail;
			});
		},
		publishCocktailFailure(state, action) {
			state.publishError = action.payload;
		},
		rateCocktail(state) {},
		rateCocktailSuccess(state) {},
		rateCocktailFailure(state, action) {
			state.rateCocktailError = action.payload;
		}
	}
});

export default cocktailSlice;