import {Route, Switch} from "react-router-dom";
import Layout from "./components/UI/Layout/Layout";
import Main from "./containers/Main/Main";
import AdminLogin from "./containers/AdminLogin/AdminLogin";
import AddCocktail from "./containers/AddCocktail/AddCocktail";
import MyCocktails from "./containers/MyCocktails/MyCocktails";
import CocktailItem from "./containers/CocktailItem/CocktailItem";

const App = () => {
	return (
		<Layout>
			<Switch>
				<Route path="/" exact component={Main}/>
				<Route path="/admin" component={AdminLogin}/>
				<Route path="/add" component={AddCocktail}/>
				<Route path="/my-cocktails" component={MyCocktails}/>
				<Route path="/cocktails/:id" component={CocktailItem}/>
			</Switch>
		</Layout>
	);
};

export default App;
