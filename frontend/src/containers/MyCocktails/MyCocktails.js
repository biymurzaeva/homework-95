import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchUserCocktails} from "../../store/actions/cocktailsActions";
import {Container, Grid, makeStyles, Typography} from "@material-ui/core";
import Cocktail from "../../components/Cocktail/Cocktail";

const useStyles = makeStyles(theme => ({
	cardGrid: {
		paddingTop: theme.spacing(8),
		paddingBottom: theme.spacing(8),
	},
}));

const MyCocktails = () => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const user = useSelector(state => state.users.user);
	const cocktails = useSelector(state => state.cocktails.userCocktails);
	const error = useSelector(state => state.cocktails.fetchUserCocktailError)

	useEffect(() => {
		dispatch(fetchUserCocktails(user._id));
	}, [dispatch, user]);

	return user ?
		<Container className={classes.cardGrid} maxWidth="md">
			<Grid container spacing={4}>
				{error && <Typography variant="h6">{error}</Typography>}
				{cocktails && cocktails.map(cocktail => (
					<Cocktail
						key={cocktail._id}
						id={cocktail._id}
						image={cocktail.image}
						name={cocktail.name}
						publish={cocktail.published}
					/>
				))}
			</Grid>
		</Container> : <Typography component="h1" variant="h6">You need login</Typography>
};

export default MyCocktails;