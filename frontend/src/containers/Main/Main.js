import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Button, Container, Grid, makeStyles} from "@material-ui/core";
import {Link} from "react-router-dom";
import {fetchCocktails} from "../../store/actions/cocktailsActions";
import Cocktail from "../../components/Cocktail/Cocktail";

const useStyles = makeStyles(theme => ({
	cardGrid: {
		paddingTop: theme.spacing(8),
		paddingBottom: theme.spacing(8),
	},
}));

const Main = () => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const user = useSelector(state => state.users.user);
	const cocktails = useSelector(state => state.cocktails.cocktails);

	useEffect(() => {
		dispatch(fetchCocktails());
	}, [dispatch]);

	return (
		<>
			{user && <div><Button component={Link} to='/add'>Add</Button></div>}
			<Container className={classes.cardGrid} maxWidth="md">
				<Grid container spacing={4}>
					{(user && cocktails) ? cocktails.map(cocktail => (
						<Cocktail
							key={cocktail._id}
							id={cocktail._id}
							image={cocktail.image}
							name={cocktail.name}
							publish={cocktail.published}
							role={user.role}
						/>
					)) : cocktails.map(cocktail => (
						<Cocktail
							key={cocktail._id}
							id={cocktail._id}
							image={cocktail.image}
							name={cocktail.name}
							publish={cocktail.published}
						/>
					))}
				</Grid>
			</Container>
		</>
	);
};

export default Main;