import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import CocktailForm from "../../components/CoctailForm/CocktailForm";
import {Typography} from "@material-ui/core";
import {createCocktail} from "../../store/actions/cocktailsActions";
import {clearErrorUser} from "../../store/actions/usersActions";

const AddCocktail = () => {
	const user = useSelector(state => state.users.user);
	const dispatch = useDispatch();
	const error = useSelector(state => state.cocktails.createCocktailError);
	const loading = useSelector(state => state.cocktails.createCocktailLoading);

	useEffect(() => {
		return () => {
			dispatch(clearErrorUser());
		};
	}, [dispatch]);

	const cocktailSubmit = cocktailData => {
		dispatch(createCocktail(cocktailData));
	};

	return user ?
		<div>
			<CocktailForm
				error={error}
				loading={loading}
				onSubmit={cocktailSubmit}
			/>
		</div>
	 : <Typography component="h1" variant="h6">Access is denied</Typography>
};

export default AddCocktail;