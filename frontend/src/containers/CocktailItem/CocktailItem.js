import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchCocktail, rateCocktail} from "../../store/actions/cocktailsActions";
import {Grid, makeStyles, Typography} from "@material-ui/core";
import {apiURL} from "../../config";
import {Rating} from "@material-ui/lab";

const useStyles = makeStyles(theme => ({
	img: {
		maxWidth: '250px',
		height: 'auto'
	},
	root: {
		display: 'flex',
		flexDirection: 'column',
		'& > * + *': {
			marginTop: theme.spacing(1),
		},
	},
}));

const CocktailItem = ({match}) => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const user = useSelector(state => state.users.user);
	const cocktail = useSelector(state => state.cocktails.cocktail);
	const error = useSelector(state => state.cocktails.fetchCocktailError);

	useEffect(() => {
		dispatch(fetchCocktail(match.params.id));
	}, [dispatch, match]);

	let cocktailImage = null;

	if (cocktail.image) {
		cocktailImage = apiURL + '/' + cocktail.image;
	}

	let star = 0;

	if (cocktail && cocktail.rating) {
		cocktail.rating && cocktail.rating.forEach(r => {
			if (r.user === user._id) {
				star = r.rating;
			}
		})
	}

	return (cocktail && star >= 0) && (
		<>
			{error && <p>{error}</p>}
			<Grid container>
				<Grid item xs={5}>
					<img src={cocktailImage} alt={cocktail.name} className={classes.img}/>
				</Grid>
				<Grid item xs={6}>
					<Typography component="h1" variant="h5">{cocktail.name}</Typography>
					<Typography component="h1" variant="h6">Rating: {cocktail.ratingScale} ({cocktail.votes} votes)</Typography>
					<ul>
						{cocktail.ingredients && cocktail.ingredients.map((ing, i) => (
							<li key={i}>
								<Typography variant="subtitle1">{ing.title} - {ing.amount}</Typography>
							</li>
						))}
					</ul>
					<Typography variant="subtitle1">{cocktail.recipe}</Typography>
					<div className={classes.root}>
						<Rating
							name="size-large"
							defaultValue={0 || star}
							size="large"
							onChange={(event, value) =>
								dispatch(rateCocktail({id: cocktail._id, rate: {rate: value}}))
							}
						/>
					</div>
				</Grid>
			</Grid>
		</>
	);
};

export default CocktailItem;