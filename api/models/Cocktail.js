const mongoose = require('mongoose');
const idvalidator = require('mongoose-id-validator');

const IngredientSchema = new mongoose.Schema({
	title: {
		type: String,
		required: true,
	},
	amount: {
		type: String,
		required: true,
	},
});

const RatingSchema = new mongoose.Schema({
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
	},
	rating: {
		type: Number,
		min: 1,
		max: 5,
	},
});

const CocktailSchema = new mongoose.Schema({
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: true,
	},
	name: {
		type: String,
		required: true,
	},
	image: {
		type: String,
		required: true,
	},
	recipe: {
		type: String,
		required: true,
	},
	ingredients: {
		type: [IngredientSchema],
	},
	published: {
		type: Boolean,
		required: true,
		default: false,
		enum: [true, false]
	},
	rating: [RatingSchema],
	votes: Number,
	ratingScale: Number,
});

CocktailSchema.plugin(idvalidator);
const Cocktail = mongoose.model('Cocktail', CocktailSchema);
module.exports = Cocktail;