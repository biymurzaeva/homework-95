const express = require('express');
const Cocktail = require('../models/Cocktail');
const multer = require("multer");
const config = require("../config");
const {nanoid} = require("nanoid");
const path = require("path");
const auth = require("../middleware/auth");
const User = require("../models/User");
const permit = require("../middleware/permit");

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
	try {
		const token =  req.get('Authorization');
		const user = await User.findOne({token});

		const query = {};

		const cocktails = await Cocktail.find({published: true});

		if (cocktails.length !== 0) {
			if (!token || !user) {
				return res.send(cocktails);
			}

			if (user && (user.role === 'admin')) {
				const allCocktails = await Cocktail.find();
				return res.send(allCocktails);
			}

			if (user && (user.role === 'user')) {

				if (req.query.user) {
					query.user = req.query.user;
					const userCocktails = await Cocktail.find(query);

					if (userCocktails.length === 0) {
						return res.status(404).send({error: 'Cocktails not found'})
					}
					return res.send(userCocktails);
				}

				return res.send(cocktails);
			}

			res.send(cocktails);
		} else {
			res.status(404).send({error: 'Cocktails not found'});
		}
	} catch (error) {
		res.sendStatus(500);
	}
});

router.get('/:id', async (req, res) => {
	try {
		const cocktail = await Cocktail.findById(req.params.id);

		if (!cocktail) {
			return res.status(404).send({error: 'Cocktail not found'});
		}

		res.send(cocktail);
	} catch (error) {
		res.sendStatus(500);
	}
});

router.post('/', auth, upload.single('image'), async (req, res) => {
	try {
		if (req.query.cocktail) {
			const cocktail = await Cocktail.findById(req.query.cocktail);

			if (!cocktail) {
				return res.status(404).send('Cocktail not found');
			}

			const index = cocktail.rating.findIndex(r => r.user.toString() === req.user._id.toString());
			const sum = cocktail.rating.reduce((acc, b) => acc + (b['rating'] || 0), 0);
			const votes = cocktail.rating.length;
			const rating = (sum/votes);

			if (index !== -1) {
			const rateId = cocktail.rating[index]._id;
				const rateCocktail = await Cocktail.findOneAndUpdate({_id: req.query.cocktail, rating: {$elemMatch: {_id: rateId}}},
					{$set: {'rating.$.user': req.user._id,
							'rating.$.rating': req.body.rate,}},
					{new: true,
						runValidators: true,});



				await Cocktail.findByIdAndUpdate(req.query.cocktail, {votes: votes, ratingScale: rating},
					{new: true,
						runValidators: true,});

				return res.send(rateCocktail);
			}

			const rateCocktail = await Cocktail.findByIdAndUpdate(
				req.query.cocktail,
				{$push: {rating: {user: req.user._id, rating: req.body.rate}}},
				{
					new: true,
					runValidators: true,
				});

			await Cocktail.findByIdAndUpdate(req.query.cocktail, {votes: votes, ratingScale: rating},
				{new: true,
					runValidators: true,});

			return res.send(rateCocktail);
		}

		const cocktailData = {
			user: req.user._id,
			name: req.body.name,
			recipe: req.body.recipe,
			ingredients: JSON.parse(req.body.ingredients),
			rating: req.body.rating || null,
		}

		if (req.file) {
			cocktailData.image = 'uploads/' + req.file.filename;
		}

		const cocktail = new Cocktail(cocktailData);
		await cocktail.save();
		res.send(cocktail);
	} catch (error) {
		res.status(400).send(error);
	}
});

router.put('/:id', auth, permit('admin'), async (req, res) => {
	try {
		const cocktail  = await Cocktail.findById(req.params.id);

		if (!cocktail) {
			return res.status(404).send({error: 'Cocktail not found'});
		}

		const updateCocktail = await Cocktail.findByIdAndUpdate(req.params.id, {published: true}, {
			new: true,
			runValidators: true,
		});

		res.send(updateCocktail);
	} catch (error) {
		res.sendStatus(500);
	}
});

router.delete('/:id', auth, permit('admin'), async (req, res) => {
	try {
		const cocktail  = await Cocktail.findById(req.params.id);

		if (!cocktail) {
			return res.status(404).send({error: 'Cocktail not found'});
		}

		await Cocktail.findByIdAndDelete(req.params.id);

		res.send('Cocktail removed');
	} catch (error) {
		res.sendStatus(500);
	}
});

module.exports = router;