const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");
const Cocktail = require("./models/Cocktail");

const run = async () => {
  await mongoose.connect(config.db.url);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const admin = await User.create({
    email: 'admin@gmail.com',
    password: 'admin',
    token: nanoid(),
	  role: 'admin',
	  displayName: 'Admin',
	  avatar: "fixture/admin.png"
  });

  await Cocktail.create({
	  user: admin,
	  name: 'Margarita',
	  ingredients: [
		  {title: "Tequila", amount: '50ml'},
		  {title: "Triple sec", amount: '20ml'},
		  {title: "Lime juice", amount: '25ml'},
		  {title: "Salt", amount: 'some'},
		  {title: "Ice", amount: '4-5 cubes'}
	  ],
	  recipe: 'Rub the rim of the glass with the lime slice to make the salt stick to it. Take care to moisten only the' +
		  ' outer rim and sprinkle the salt on it. The salt should present to the lips of the imbiber and never mix into' +
		  ' the cocktail. Shake the other ingredients with ice, then carefully pour into the glass.',
	  image: 'fixture/margarita.jpg',
	  published: true,
	  rating: [],
	  ratingScale: 0,
	  votes: 0,
  }, {
  	user: admin,
	  name: 'Dirty Martini',
	  ingredients: [
		  {title: 'Dry Vermouth', amount: '10ml'},
		  {title: 'Gin', amount: '50ml'},
		  {title: 'Olive Brine', amount: '15ml'},
		  {title: 'Green Olives', amount: '3 pieces'},
	  ],
	  recipe: 'Add the gin or vodka, vermouth and olive brine to a mixing glass filled with ice and stir until ' +
		  'well-chilled. Strain into a chilled cocktail glass.Garnish with a skewer of olives.',
	  image: 'fixture/martini.jpg',
	  published: false,
	  rating: [],
	  ratingScale: 0,
	  votes: 0,
  }, {
  	user: admin,
	  name: 'Long Island iced tea',
	  ingredients: [
		  {title: 'Vanilla vodka', amount: '50ml'},
		  {title: 'London dry gin', amount: '50ml'},
		  {title: 'Reposado tequila', amount: '50ml'},
		  {title: 'Rum', amount: '50ml'},
		  {title: 'Triple sec', amount: '50ml'},
		  {title: 'Lime juice', amount: '50-100ml'},
		  {title: 'Ice', amount: '6-8 cubes'},
		  {title: 'cola', amount: '500ml'},
		  {title: 'Limes', amount: '2 limes, cut into wedges'},
	  ],
	  recipe: 'Pour the vodka, gin, tequila, rum and triple sec into a large (1.5l) jug, and add lime juice to taste.' +
		  ' Half fill the jug with ice, then stir until the outside feels cold. Add the cola then stir to combine. ' +
		  'Drop in the lime wedges. Fill 4 tall glasses with more ice cubes and pour in the iced tea.',
	  image: 'fixture/long-island-iced-tea.jpg',
	  published: false,
	  rating: [],
	  ratingScale: 0,
	  votes: 0,
  });

  await mongoose.connection.close();
};

run().catch(console.error);